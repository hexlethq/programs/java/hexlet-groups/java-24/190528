### Hexlet tests and linter status:
[![Actions Status](https://github.com/saymon-says/java-project-lvl5/workflows/hexlet-check/badge.svg)](https://github.com/saymon-says/java-project-lvl5/actions)
[![Actions Status](https://github.com/saymon-says/java-project-lvl5/workflows/my-project-check/badge.svg)](https://github.com/saymon-says/java-project-lvl5/actions)
[![Maintainability](https://api.codeclimate.com/v1/badges/a86105c3641322266001/maintainability)](https://codeclimate.com/github/saymon-says/java-project-lvl5/maintainability)

https://to-do-my-list.herokuapp.com/