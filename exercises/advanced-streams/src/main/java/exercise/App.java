package exercise;

import java.util.Arrays;
import java.util.stream.Collectors;

// BEGIN
public class App {

    final static String KEYS = "X_FORWARDED_";
    final static String START = "envi";

    public static String getForwardedVariables(final String content) {
        return content.lines()
                .filter(x -> x.startsWith(START) && x.contains(KEYS))
                .map(x -> x.substring(x.indexOf("\"") + 1, x.lastIndexOf("\"")))
                .flatMap(x -> Arrays.stream(x.split(",")))
                .filter(x -> x.contains(KEYS))
                .map(x -> x.substring(x.lastIndexOf("_") + 1))
                .collect(Collectors.joining(","));
    }
}
//END
