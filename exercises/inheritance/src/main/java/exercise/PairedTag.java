package exercise;

import java.util.List;
import java.util.Map;

// BEGIN
public class PairedTag extends Tag {

    String txt;
    List<Tag> tags;

    public PairedTag(String name, Map<String, String> attributes, String txt, List<Tag> tags) {
        super(name, attributes);
        this.txt = txt;
        this.tags = tags;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Tag tag : tags) {
            stringBuilder.append(tag.toString());
        }
        return String.format("<%s%s>%s%s</%s>", getName(), stringifyAttributes(), txt, stringBuilder, getName());
    }
}
// END
