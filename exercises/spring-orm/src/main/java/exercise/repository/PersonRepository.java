package exercise.repository;

import exercise.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

    Person findById(long id);

}
