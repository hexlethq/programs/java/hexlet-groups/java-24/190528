package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String stars = "*";

        int length = cardNumber.length();
        String starsCountStr = stars.repeat(starsCount);
        String str = starsCountStr + cardNumber.substring(length - 4);

        System.out.println(str);
        // END
    }
}
