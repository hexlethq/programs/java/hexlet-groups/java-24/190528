package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        String str;
        String trimStr;
        trimStr = sentence.trim();
        String lastChar = trimStr.substring(trimStr.length() - 1);
        if (lastChar.equals("!")) {
            str = trimStr.toUpperCase();
        } else {
            str = trimStr.toLowerCase();
        }
        System.out.println(str);
        // END
    }
}
