package exercise;

import java.util.ArrayList;
import java.util.Arrays;


// BEGIN
public class Kennel {

    private static final ArrayList<String[]> PUPPIES = new ArrayList<>();

    public static void addPuppy(final String[] puppy) {
        PUPPIES.add(puppy);
    }

    public static void addSomePuppies(final String[][] somePuppies) {
        PUPPIES.addAll(Arrays.asList(somePuppies));
    }

    public static int getPuppyCount() {
        return PUPPIES.size();
    }

    public static boolean isContainPuppy(final String puppyName) {
        for (int i = 0; i < getPuppyCount(); i++) {
            if (PUPPIES.get(i)[0].equals(puppyName)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return PUPPIES.toArray(new String[0][]);
    }

    public static String[] getNamesByBreed(final String breed) {
        ArrayList<String> listNameOfBreed = new ArrayList<>();
        for (int i = 0; i < getPuppyCount(); i++) {
            if (PUPPIES.get(i)[1].equals(breed)) {
                listNameOfBreed.add(PUPPIES.get(i)[0]);
            }
        }
        return listNameOfBreed.toArray(new String[0]);
    }

    public static void resetKennel() {
        PUPPIES.clear();
    }

    public static boolean removePuppy(final String namePuppy) {
        if (isContainPuppy(namePuppy)) {
            for (int i = 0; i < PUPPIES.size(); i++) {
                if (PUPPIES.get(i)[0].equals(namePuppy)) {
                    PUPPIES.remove(i);
                }
            }
            return true;
        }
        return false;
    }
}
// END
