package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] num) {
        boolean isSorted = false;
        int tempElement;

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < num.length - 1; i++) {
                if (num[i] > num[i + 1]) {
                    isSorted = false;
                    tempElement = num[i];
                    num[i] = num[i + 1];
                    num[i + 1] = tempElement;
                }
            }
        }
        return num;
    }

    public static int[] sortSelect(int[] arr) {

        return arr;
    }
    // END
}
