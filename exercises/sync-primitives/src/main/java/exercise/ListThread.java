package exercise;

import java.util.Random;

// BEGIN
public class ListThread extends Thread {
    private final SafetyList safetyList;

    private static final int WAITING_MS = 1;

    private static final int COUNT_NUMBERS = 1000;

    private final Random random = new Random();

    public ListThread(SafetyList safetyList) {
        this.safetyList = safetyList;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_NUMBERS; i++) {
            try {
                Thread.sleep(WAITING_MS);
                this.safetyList.add(random.nextInt());
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
// END
