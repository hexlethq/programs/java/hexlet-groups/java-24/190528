package exercise;

import java.util.ArrayList;
import java.util.List;

class SafetyList {
    // BEGIN
    private List<Integer> safetyList = new ArrayList<>();

    public synchronized void add(int number) {
        safetyList.add(number);
    }

    public int get(int index) {
        return safetyList.get(index);
    }

    public int getSize() {
        return safetyList.size();
    }
    // END
}
