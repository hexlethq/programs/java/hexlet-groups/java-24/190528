package exercise;

class App {

    public static void main(String[] args) {
        // BEGIN
        SafetyList safetyList = new SafetyList();
        Thread first = new ListThread(safetyList);
        Thread second = new ListThread(safetyList);

        first.start();
        second.start();

        try {
            first.join();
            second.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(safetyList.getSize());
        // END
    }
}

