package exercise;

import java.util.LinkedHashMap;
import java.util.Map;

// BEGIN
public class App {

    private static final String UNCHANGED = "unchanged";
    private static final String DELETED = "deleted";
    private static final String ADDED = "added";
    private static final String CHANGED = "changed";

    public static LinkedHashMap<String, String> genDiff(final Map<String, Object> first,
                                                        final Map<String, Object> second) {

        Map<String, String> result = new LinkedHashMap<>();

        for (Map.Entry<String, Object> firstMap : first.entrySet()) {
            String key = firstMap.getKey();
            if (second.containsKey(key) && firstMap.getValue().equals(second.get(key))) {
                result.put(key, UNCHANGED);
            } else if (second.containsKey(key) && !firstMap.getValue().equals(second.get(key))) {
                result.put(key, CHANGED);
            } else {
                result.put(key, DELETED);
            }
        }
        for (Map.Entry<String, Object> secondMap : second.entrySet()) {
            String key = secondMap.getKey();
            if (first.containsKey(key) && secondMap.getValue().equals(first.get(key))) {
                result.put(key, UNCHANGED);
            } else if (first.containsKey(key) && !secondMap.getValue().equals(first.get(key))) {
                result.put(key, CHANGED);
            } else {
                result.put(key, ADDED);
            }
        }


        return (LinkedHashMap<String, String>) result;
    }

}
//END
