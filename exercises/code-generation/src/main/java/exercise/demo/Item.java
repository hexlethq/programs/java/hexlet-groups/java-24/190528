package exercise.demo;

import lombok.Builder;

//@ToString(of = "id")
//@EqualsAndHashCode(of = "id")
//@Getter
//@Setter

//@Data

@Builder
public class Item {
    //@NonNull
    private Integer id;
    private String name;
    private Double price;
}
