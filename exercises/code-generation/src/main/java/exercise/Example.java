package exercise;

import exercise.demo.Item;

class Example {
    public static void main(String[] args) {
        //data + getter/setter/toString
//        Item milk = new Item();
//        milk.setId(1);
//        milk.setName("Молоко");
//        milk.setPrice(99.90);
//        System.out.println(milk);
//
//        Item milk2 = new Item();
//        milk2.setId(1);
//        milk2.setName("Молоко");
//        milk2.setPrice(99.90);
//
//        System.out.println(milk.equals(milk2));


//        // builder
//        Item item = Item.builder()
//                .id(1)
//                .name("Котлета")
//                .price(100.0)
//                .build();
//
//        System.out.println(item);

        // nonNull
        Item item = Item.builder()
                .price(10.0)
                .name("Йогурт")
                .build();
        System.out.println(item);

    }
}
