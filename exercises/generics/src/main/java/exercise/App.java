package exercise;

import java.util.ArrayList;
import java.util.HashMap;

// BEGIN
public class App {

    public static ArrayList<HashMap> findWhere(final ArrayList<HashMap<String, String>> books,
                                               final HashMap<String, String> where) {
        ArrayList<HashMap> result = (ArrayList<HashMap>) books.clone();
        for (HashMap<String, String> book : books) {
            for (String keys : where.values()) {
                if (!book.containsValue(keys)) {
                    result.remove(book);
                }
            }
        }
        return result;
    }
}
//END
