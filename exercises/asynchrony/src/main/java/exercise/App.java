package exercise;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;

class App {

    // BEGIN
    public static CompletableFuture<String> unionFiles(String firstPath,
                                                       String secondPath,
                                                       String resultPath) {
        CompletableFuture<String> firstInput = readFileAsync(firstPath);
        CompletableFuture<String> secondInput = readFileAsync(secondPath);

        return firstInput
                .thenCombine(secondInput, (dataFirst, dataSecond) -> dataFirst + dataSecond)
                .thenCompose(data -> writeFileAsync(data, resultPath))
                .exceptionally(ex -> {
                    System.out.println(ex.getMessage());
                    return null;
                });
    }
    // END

    public static void main(String[] args) throws Exception {
        // BEGIN
        unionFiles("src/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/result.txt")
                .get();

        // END
    }

    private static CompletableFuture<String> readFileAsync(String path) {
        return CompletableFuture
                .supplyAsync(() -> {
                    try {
                        return Files.readString(Paths.get(path).toAbsolutePath().normalize());
                    } catch (NoSuchFileException e) {
                        throw new RuntimeException(e.getClass().getSimpleName() + " - " + e.getMessage());
                    } catch (IOException e) {
                        throw new RuntimeException(e.getMessage());
                    }
                });
    }

    private static CompletableFuture<String> writeFileAsync(String data, String path) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Files.writeString(Paths.get(path).toAbsolutePath().normalize(), data);
                return data;
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        });
    }

    public static CompletableFuture<Long> getDirectorySize(String path) {
        return CompletableFuture
                .supplyAsync(() -> {
                    Path folder = Paths.get(path).toAbsolutePath().normalize();
                    try {
                        return Files.walk(folder)
                                .filter(x -> x.toFile().isFile())
                                .mapToLong(x -> x.toFile().length())
                                .sum();

                    } catch (IOException e) {
                        throw new RuntimeException(e.getMessage());
                    }
                });
    }

}

