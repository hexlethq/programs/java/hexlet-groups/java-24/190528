package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String fullName) {
        StringBuilder shortName = new StringBuilder();
        String[] str = fullName.split(" ");
        for (String s : str) {
            shortName.append(s.charAt(0));
        }
        return shortName.toString().toUpperCase();
    }
    // END
}
