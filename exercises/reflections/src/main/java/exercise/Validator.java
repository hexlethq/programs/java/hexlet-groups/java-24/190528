package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {

    public static List<String> validate(Address address) {

        Field[] fields = address.getClass().getDeclaredFields();

        List<String> listOfNullField = new ArrayList<>();

        for (Field field : fields) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            if (notNull != null) {
                field.setAccessible(true);
                try {
                    if (field.get(address) == null) {
                        listOfNullField.add(field.getName());
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return listOfNullField;
    }

    public static Map<String, List<String>> advancedValidate(Address address) throws IllegalAccessException {

        Field[] fields = address.getClass().getDeclaredFields();

        Map<String, List<String>> mapOfNotVerifiedFields = new HashMap<>();

        for (Field field : fields) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            MinLength minLength = field.getAnnotation(MinLength.class);
            field.setAccessible(true);
            if (notNull != null) {
                if (field.get(address) == null) {
                    mapOfNotVerifiedFields.put(field.getName(), Collections.singletonList("can not be null"));
                } else if (minLength != null) {
                    String fieldLessThenMin = (String) field.get(address);
                    int length = minLength.minLength();
                    if (fieldLessThenMin.length() < length) {
                        mapOfNotVerifiedFields.put(field.getName(),
                                Collections.singletonList("length less then " + length));
                    }
                }
            }
        }
        return mapOfNotVerifiedFields;
    }
}
// END
