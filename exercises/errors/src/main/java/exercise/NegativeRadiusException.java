package exercise;

// BEGIN
public class NegativeRadiusException extends Exception {

//    String str;

    public NegativeRadiusException(String str) {
        super(str);
    }
}
// END
