package exercise;

// BEGIN
public class App {

    public static void main(String[] args){
        printSquare(new Circle(new Point(2, 3), -5));
    }

    public static void printSquare(Circle circle) {
        try {
            System.out.printf("%s\n", String.format("%.0f", circle.getSquare()));
        } catch (NegativeRadiusException e) {
            System.out.print("Ne udalos poschitat ploshad\n");
        } finally {
            System.out.print("Vichslenie okoncheno");
        }
    }

}
// END
