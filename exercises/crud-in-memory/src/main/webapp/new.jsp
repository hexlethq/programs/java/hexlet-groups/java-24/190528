<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add new user</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
          crossorigin="anonymous">
</head>
<body>
<div class="container">
    <a href="/users">Все пользователи</a>
    <!-- BEGIN -->
    <h4>${error}</h4>
    <form action="/users/new" method="post">
        <div class="mb-3">
            <label for="inputEmail" class="form-label">E-mail</label>
            <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" name="email" value="${email}">
        </div>
        <div class="mb-3">
            <label for="inputFirstName" class="form-label">Имя</label>
            <input type="text" class="form-control" id="inputFirstName" name="firstName" value="${firstName}">
        </div>
        <div class="mb-3">
            <label for="inputLastName" class="form-label">Фамилия</label>
            <input type="text" class="form-control" id="inputLastName" name="lastName" value="${lastName}">
        </div>
        <button type="submit" class="btn btn-primary">Добавить</button>
    </form>
    <!-- END -->
</div>
</body>
</html>
