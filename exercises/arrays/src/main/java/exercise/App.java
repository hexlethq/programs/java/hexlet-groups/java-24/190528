package exercise;

class App {
    //     BEGIN
    public static int[] reverse(int[] arr) {
        int[] tempArray = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            tempArray[i] = arr[arr.length - 1 - i];
        }
        return tempArray;
    }

    public static int getIndexOfMaxNegative(int[] arrays) {

        int tempElement = Integer.MIN_VALUE;
        int countCycle = -1;

        for (int i = 0; i < arrays.length; i++) {
            if (arrays[i] > tempElement && arrays[i] < 0) {
                countCycle = i;
                tempElement = arrays[i];
            }
        }
        return countCycle;
    }
    // END
}
