package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private static List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper mapper = new ObjectMapper();
        return mapper.<List<Map<String, String>>>readValue(getFile("src/main/resources/users.json"),
                new TypeReference<>() {
                });
        // END
    }

    public static void showUsers(HttpServletRequest request,
                                 HttpServletResponse response)
            throws IOException {

        // BEGIN
        response.setContentType("text/html;charset=UTF-8");
        StringBuilder builder = new StringBuilder("<table>");
        List<Map<String, String>> listUsers = getUsers();
        for (Map<String, String> listUser : listUsers) {
            String id = listUser.get("id");
            String fullName = listUser.get("firstName") + " " + listUser.get("lastName");
            builder.append("<tr><td>").append(id).append("</td><td><a href=\"/users/").append(id).append("\">")
                    .append(fullName).append("</a></td></tr>");
        }
        builder.append("</table>");
        PrintWriter out = response.getWriter();
        out.println(builder);
        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        StringBuilder builder = new StringBuilder("<table>");
        List<Map<String, String>> listUsers = getUsers();
        if (id.equals("") || Integer.parseInt(id) > listUsers.size()) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
        }
        for (int i = 0; i < listUsers.size(); i++) {
            if (listUsers.get(i).get("id").equals(id)) {
                String firstName = listUsers.get(i).get("firstName");
                String lastName = listUsers.get(i).get("lastName");
                String email = listUsers.get(i).get("email");

                builder.append("<tr><td>").append(id).append("</td><td>")
                        .append(firstName).append("</td><td>").append(lastName)
                        .append("</td><td>").append(email).append("</td>");
            }
        }
        builder.append("</table>");
        PrintWriter pw = response.getWriter();
        pw.println(builder);
        // END
    }

    public static String getFile(String fileName) throws IOException {
        return Files.readString(getFixturePath(fileName));
    }

    public static Path getFixturePath(String fileName) {
        return Paths.get(fileName).toAbsolutePath().normalize();
    }
}
