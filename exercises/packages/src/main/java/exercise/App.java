// BEGIN
package exercise;

import exercise.geometry.Point;

import static exercise.geometry.Segment.getBeginPoint;
import static exercise.geometry.Segment.getEndPoint;

public class App {

    public static double[] getMidpointOfSegment(final double[][] segment) {
        double point1x = Point.getX(getBeginPoint(segment));
        double point1y = Point.getY(getBeginPoint(segment));

        double point2x = Point.getX(getEndPoint(segment));
        double point2y = Point.getY(getEndPoint(segment));

        return new double[]{(point1x + point2x) / 2, (point1y + point2y) / 2};

    }

    public static double[][] reverse(final double[][] segment) {

        double point1x = Point.getX(getBeginPoint(segment));
        double point1y = Point.getY(getBeginPoint(segment));

        double point2x = Point.getX(getEndPoint(segment));
        double point2y = Point.getY(getEndPoint(segment));

        return new double[][]{{point2x, point2y}, {point1x, point1y}};
    }

    /*
    самостоятельная работа
     */

    public static boolean isBelongToOneQuadrant(final double[][] segment) {

        return getQuadrant(getBeginPoint(segment)) == getQuadrant(getEndPoint(segment));
    }

    private static int getQuadrant(double[] point) {

        if (Point.getX(point) > 0 && Point.getY(point) > 0) {
            return 1;
        }

        if (Point.getX(point) > 0 && Point.getY(point) < 0) {
            return 4;
        }

        if (Point.getX(point) < 0 && Point.getY(point) < 0) {
            return 3;
        }
        return 2;
    }

}
// END
