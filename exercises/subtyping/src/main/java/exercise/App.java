package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
public class App {

    public static void swapKeyValue(KeyValueStorage storage) {

        Map<String, String> swapped = new HashMap<>();

        for (Map.Entry<String, String> maps : storage.toMap().entrySet()) {
            swapped.put(maps.getValue(), maps.getKey());
            storage.unset(maps.getKey());
        }

        for (Map.Entry<String, String> swapDictionary : swapped.entrySet()) {
            storage.set(swapDictionary.getKey(), swapDictionary.getValue());
        }
    }

}
// END
