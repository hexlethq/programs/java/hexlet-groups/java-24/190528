package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class App {
    public static void main(String[] args) {
        System.out.println(toString(getWordCount("java java nap gap cat nap")));
    }

    public static HashMap<String, Integer> getWordCount(final String sentence) {

        HashMap<String, Integer> countWords = new HashMap<>();

        String[] wordsArray = sentence.split(" ");

        if (sentence.equals("")) {
            return countWords;
        }
        for (String word : wordsArray) {
            Integer count = countWords.getOrDefault(word, 0);
            countWords.put(word, count + 1);
        }
        return countWords;
    }

    public static String toString(final HashMap<String, Integer> wordCount) {
        if (wordCount.size() == 0) {
            return "{}";
        }
        StringBuilder builder = new StringBuilder("{\n");

        for (Map.Entry<String, Integer> wordToCount : wordCount.entrySet()) {
            String word = wordToCount.getKey();
            Integer count = wordToCount.getValue();
            builder.append("  ")
                    .append(word)
                    .append(": ")
                    .append(count)
                    .append("\n");
        }

        builder.append("}");
        return String.valueOf(builder);
    }
}
//END
