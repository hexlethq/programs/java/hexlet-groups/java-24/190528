package exercise;

class App {
//    public static void main(String[] args) {
//        isBigOdd(1002);
//        sayEvenOrNot(21);
//        printPartOfHour(45);
//    }
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = false;
        if ((number >= 1001) && (number % 2 != 0)) {
            isBigOddVariable = true;
        }
//        System.out.println(isBigOddVariable);
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 != 0) {
            System.out.println("no");
        }
        if (number % 2 == 0) {
            System.out.println("yes");
        }
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (0 <= minutes && minutes <= 14) {
            System.out.println("First");
        }
        if (15 < minutes && minutes <= 30) {
            System.out.println("Second");
        }
        if (30 < minutes && minutes <= 45) {
            System.out.println("Third");
        }
        if (45 < minutes && minutes <= 59) {
            System.out.println("Fourth");
        }
        // END
    }
}
