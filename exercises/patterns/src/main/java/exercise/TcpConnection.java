package exercise;
import exercise.connections.Connected;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

import java.util.List;
import java.util.ArrayList;

// BEGIN
public class TcpConnection {

    private Connection connection;
    private String ip_address;
    private int port;

    public TcpConnection(String ip_address, int port) {
        this.ip_address = ip_address;
        this.port = port;
        this.connection = new Disconnected(this);
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void connect() {
        this.connection.connect();
    }

    public void disconnect() {
        this.connection.disconnect();
    }

    public void write(String str) {
        this.connection.write(str);
    }

    public String getCurrentState() {
        return this.connection.getCurrentState();
    }
}
// END
