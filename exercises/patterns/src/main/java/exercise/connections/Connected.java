package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection {

    private TcpConnection tcpConnection;

    public Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error! Try to connect when connection already established.");
    }

    @Override
    public void disconnect() {
        TcpConnection tcp = this.tcpConnection;
        tcp.setConnection(new Disconnected(tcp));
        System.out.println("disconnect");
    }

    @Override
    public void write(String str) {
        if (this.tcpConnection.getCurrentState().equals("disconnect")) {
            System.out.println("Error! Try to write to disconnected connection.");
        } else {
            System.out.println(str);
        }
    }
}
// END
