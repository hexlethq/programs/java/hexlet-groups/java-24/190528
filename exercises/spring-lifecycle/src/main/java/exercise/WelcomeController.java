package exercise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

// BEGIN
@RestController
public class WelcomeController {

    @Autowired
    private Meal meal;

    @Autowired
    private MyApplicationConfig myApplicationConfig;

    @GetMapping(path = "/daytime")
    public String getText() {
        return String.format("It is %s now. Enjoy your %s",
                myApplicationConfig.daytime().getName(),
                meal.getMealForDaytime(myApplicationConfig.daytime().getName()));
    }
}
// END
