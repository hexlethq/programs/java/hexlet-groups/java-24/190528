package exercise;

import exercise.daytimes.Day;
import exercise.daytimes.Daytime;
import exercise.daytimes.Evening;
import exercise.daytimes.Morning;
import exercise.daytimes.Night;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

// BEGIN
@Configuration
public class MyApplicationConfig {
    @Bean
    public Daytime daytime() {
        int localHour = LocalDateTime.now().getHour();
        if (6 <= localHour && localHour < 12) {
            return new Morning();
        }
        if (12 <= localHour && localHour < 18) {
            return new Day();
        }
        if (18 <= localHour && localHour < 23) {
            return new Evening();
        } else {
            return new Night();
        }
    }
}
// END
