package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {

    public static String[][] duplicateValues(final String[][] arr) {
        return Arrays.stream(arr)
                .flatMap(x -> Stream.of(x, x))
                .map(x -> {
                    String[] strings = new String[x.length * 2];
                    for (int i = 0; i < x.length; i++) {
                        strings[i * 2] = x[i];
                        strings[i * 2 + 1] = x[i];
                    }
                    return strings;
                })
                .toArray(String[][]::new);
    }
}
// END
