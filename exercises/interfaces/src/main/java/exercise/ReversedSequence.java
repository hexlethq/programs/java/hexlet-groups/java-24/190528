package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {

    String str;

    public ReversedSequence(String str) {
        this.str = reverse(str);
    }

    @Override
    public int length() {
        return str.length();
    }

    @Override
    public char charAt(int index) {
        return str.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return str.substring(start, end);
    }

    public static String reverse(String str) {
        return new StringBuilder(str).reverse().toString();
    }

    @Override
    public String toString() {
        return new StringBuilder(str).toString();
    }
}
// END
