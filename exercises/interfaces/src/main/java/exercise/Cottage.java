package exercise;

// BEGIN
public class Cottage implements Home {

    double area;
    int floors;

    public Cottage(double area, int floors) {
        this.area = area;
        this.floors = floors;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public int compareTo(Home etc) {
        if (etc.getArea() > this.getArea()) {
            return -1;
        } else if (etc.getArea() < this.getArea()) {
            return 1;
        } return 0;
    }

    @Override
    public String toString() {
        return floors + " этажный коттедж площадью " + getArea() + " метров";
    }
}
// END
