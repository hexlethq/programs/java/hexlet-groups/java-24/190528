package exercise;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

// BEGIN
public class App {

    public static void main(String[] args) {
        List<Home> appartments = new ArrayList<>(List.of(
                new Cottage(100, 1),
                new Flat(190, 10, 2),
                new Flat(180, 30, 5),
                new Cottage(250, 3)
        ));
        System.out.println(buildAppartmentsList(appartments, 2));
    }

    public static List<String> buildAppartmentsList(List<Home> apartments, int count) {

        apartments.sort(Home::compareTo);

        List<String> sorted = new LinkedList<>();
        if (!(apartments.size() == 0)) {
            for (int i = 0; i < count; i++) {
                sorted.add(apartments.get(i).toString());
            }
        }

        return sorted;
    }


}
// END
