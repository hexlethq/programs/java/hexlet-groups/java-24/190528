package exercise.controllers;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.core.validation.ValidationError;
import io.javalin.http.Handler;

import java.util.List;
import java.util.Map;

import io.javalin.core.validation.Validator;

import exercise.domain.User;
import exercise.domain.query.QUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.eclipse.jetty.util.StringUtil;

public final class UserController {

    public static Handler listUsers = ctx -> {

        List<User> users = new QUser()
                .orderBy()
                .id.asc()
                .findList();

        ctx.attribute("users", users);
        ctx.render("users/index.html");
    };

    public static Handler showUser = ctx -> {
        long id = ctx.pathParamAsClass("id", Long.class).getOrDefault(null);

        User user = new QUser()
                .id.equalTo(id)
                .findOne();

        ctx.attribute("user", user);
        ctx.render("users/show.html");
    };

    public static Handler newUser = ctx -> {

        ctx.attribute("errors", Map.of());
        ctx.attribute("user", Map.of());
        ctx.render("users/new.html");
    };

    public static Handler createUser = ctx -> {
        // BEGIN
        String name = ctx.formParam("firstName");
        String lastName = ctx.formParam("lastName");
        String password = ctx.formParam("password");
        String email = ctx.formParam("email");

        Validator<String> nameValidator = ctx.formParamAsClass("firstName", String.class)
                .check(th -> !th.isEmpty(), "Name can't null");

        Validator<String> lastNameValidator = ctx.formParamAsClass("lastName", String.class)
                .check(th -> !th.isEmpty(), "LastName can't null");

        Validator<String> emailValidator = ctx.formParamAsClass("email", String.class)
                        .check(it -> EmailValidator.getInstance().isValid(email), "Email incorrect");

        Validator<String> passwordValidator = ctx.formParamAsClass("password", String.class)
                .check(StringUtils::isNumeric, "Password contain only number")
                .check(it -> (it.length() > 4), "Length password must be 4+ number");

        Map<String, List<ValidationError<? extends Object>>> errors = JavalinValidation.collectErrors(
                nameValidator,
                lastNameValidator,
                emailValidator,
                passwordValidator
        );

        if (!errors.isEmpty()) {
         ctx.status(422);
         ctx.attribute("errors", errors);
         User invalidUser = new User(name, lastName, email, password);
         ctx.attribute("user", invalidUser);
         ctx.render("users/new.html");
         return;
     } else {
            User user = new User(name, lastName, email, password);
            user.save();
            ctx.sessionAttribute("flash", "User has been successful create");
            ctx.redirect("/users");
        }
        // END
    };
}
