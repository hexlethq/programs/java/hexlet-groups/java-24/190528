package exercise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Map<String, String> inspectMapCustomBean = new HashMap<>();
    private static final String OUT_INFO = "Was called method: %s() with arguments: %s";

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (!bean.getClass().isAnnotationPresent(Inspect.class)) {
            return bean;
        }
        Inspect inspect = bean.getClass().getAnnotation(Inspect.class);
        this.inspectMapCustomBean.put(beanName, inspect.level());
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (!inspectMapCustomBean.containsKey(beanName)) {
            return bean;
        }
        String logLevel = inspectMapCustomBean.get(beanName);

        InvocationHandler handler = (proxy, method, args) -> {
            String message = String.format(OUT_INFO, method.getName(), Arrays.toString(args));
            switch (logLevel) {
                case "info" -> logger.info(message);
                case "debug" -> logger.debug(message);
                default -> throw new IllegalStateException("Unexpected value: " + logLevel);
            }
            return method.invoke(bean, args);
        };

        return Proxy.newProxyInstance(
                bean.getClass().getClassLoader(),
                bean.getClass().getInterfaces(),
                handler);
    }
}
// END
