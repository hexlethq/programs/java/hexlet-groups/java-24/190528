<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${user.get("firstName")} ${user.get("lastName")}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
          crossorigin="anonymous">
</head>
<body>
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">E-mail</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">${user.get("id")}</th>
        <td>${user.get("firstName")}</td>
        <td>${user.get("lastName")}</td>
        <td>${user.get("email")}</td>
    </tr>
    </tbody>
</table>
<form action="/users/delete" method="get">
    <button type="submit" name="id" value="${user.get("id")}" class="btn btn-outline-warning btn-lg">Удалить</button>
</form>
</body>
</html>
<!-- END -->
