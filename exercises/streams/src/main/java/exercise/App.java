package exercise;

import java.util.ArrayList;

// BEGIN
public class App {

    public static Long getCountOfFreeEmails(final ArrayList<String> email) {
        return email.stream()
                .filter(mail -> mail.contains("@gmail.com")
                        || mail.contains("@yandex.ru")
                        || mail.contains("@hotmail.com")).count();
    }

}
// END
