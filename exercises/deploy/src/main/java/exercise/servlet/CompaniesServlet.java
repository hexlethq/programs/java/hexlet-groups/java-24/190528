package exercise.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        PrintWriter pw = response.getWriter();
        if (request.getQueryString() == null || (request.getParameter("search").equals(""))) {
            for (String company : getCompanies()) {
                pw.write(company + "\n");
            }
        } else {
            String filteredCompanyList = getCompanies()
                    .stream()
                    .filter(company -> company.contains(request.getParameter("search")))
                    .collect(Collectors.joining("\n"));
            if (filteredCompanyList.isEmpty()) {
                pw.println("Companies not found");
            } else {
                pw.println(filteredCompanyList);
            }
        }
        pw.close();
        // END
    }
}
