package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = new int[2];
        point[0] = x;
        point[1] = y;
        return point;
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return "(" + getX(point) + ", " + getY(point) + ")";
    }

    public static int getQuadrant(int[] point) {
        int quadrant = 0;
        if (getX(point) > 0 && getY(point) > 0) {
            quadrant = 1;
        } else if (getX(point) > 0 && getY(point) < 0) {
            quadrant = 4;
        } else if (getX(point) < 0 && getY(point) > 0) {
            quadrant = 2;
        } else if (getX(point) < 0 && getY(point) < 0) {
            quadrant = 3;
        }
        return quadrant;
    }

    public static int[] getSymmetricalPointByX(int[] point) {
        return makePoint(getX(point), getY(point) * -1);
    }

    public static int calculateDistance(int[] point1, int[] point2) {
        double pointX1 = getX(point1);
        double pointY1 = getY(point1);
        double pointX2 = getX(point2);
        double pointY2 = getY(point2);

        return (int) Math.sqrt(Math.pow((pointX2 - pointX1), 2) + Math.pow((pointY2 - pointY1), 2));
    }
    // END
}
