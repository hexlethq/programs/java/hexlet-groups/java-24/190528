package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        getSquare(4, 5, 45);
    }

    public static double getSquare(int a, int b, int grad) {
        double rad = grad * 3.14 / 180;
        double result;

        result = 0.5 * (a * b) * Math.sin(rad);
        System.out.println(result);
        return result;
    }
    // END
}
