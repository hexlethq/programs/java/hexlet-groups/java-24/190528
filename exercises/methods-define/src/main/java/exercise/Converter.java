package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        convert(10, "b");
    }

    public static int convert(int a, String direction) {
        int result;
        if (direction.equals("Kb")) {
            result = a / 1024;
            System.out.println(a + " b = " + result + " Kb");
        } else if (direction.equals("b")) {
            result = a * 1024;
            System.out.println(a + " Kb = " + result + " b");
        } else {
            result = 0;
            System.out.println("Fatal Error! Alarm!");
        }
        return result;
    }
    // END
}
