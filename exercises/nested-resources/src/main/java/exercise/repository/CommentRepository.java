package exercise.repository;

import exercise.model.Comment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

    // BEGIN
    @Query(value = "SELECT * FROM comments WHERE post_id = ?1", nativeQuery = true)
    Iterable<Comment> findByPostId(Long postId);

    Optional<Comment> findByIdAndPostId(Long Id, Long postId);

    @Override
    boolean existsById(Long commentId);
    // END
}
