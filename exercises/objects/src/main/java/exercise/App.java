package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

class App {
    // BEGIN

    public static String buildList(final String[] strings) {
        StringBuilder sb = new StringBuilder();
        if (strings.length == 0) {
            return "";
        }
        sb.append("<ul>\n");
        for (String string : strings) {
            sb.append("  <li>");
            sb.append(string);
            sb.append("</li>\n");
        }
        sb.append("</ul>");
        return sb.toString();
    }

    public static String getUsersByYear(final String[][] users, final int year) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");
        for (String[] user : users) {
            if (user[1].contains(String.valueOf(year))) {
                sb.append("  <li>");
                sb.append(user[0]);
                sb.append("</li>\n");
            }
        }
        if (!sb.substring(0).contains("<li>")) {
            return "";
        }
        sb.append("</ul>");
        return sb.toString();
    }

    public static String getYoungestUser(final String[][] users, final String searchDate) {

        String userName = "";
        LocalDate searchDateOfUsers = LocalDate.parse(searchDate, DateTimeFormatter
                .ofPattern("dd MMM yyyy", Locale.ENGLISH));
        String searchDateOfUsersBirthDay = searchDateOfUsers
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String now = "1970-01-01";

        for (String[] user : users) {
            if (isAfterDate(searchDateOfUsersBirthDay, user[1])) {
                if (!isAfterDate(now, user[1])) {
                    userName = user[0];
                    now = user[1];
                }
            }
        }
        return userName;
    }

    private static boolean isAfterDate(String searchDate, String usersDate) {
        LocalDate date = LocalDate.parse(searchDate, DateTimeFormatter
                .ofPattern("yyyy-MM-dd"));
        LocalDate userBirthDay = LocalDate.parse(usersDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return date.isAfter(userBirthDay);
    }
    // END
}
