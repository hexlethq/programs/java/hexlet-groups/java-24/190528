// BEGIN
package exercise;

import com.google.gson.Gson;

class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

    public static String toJson(final String[] strings) {
        Gson gson = new Gson();
        System.out.println(gson.toJson(strings));
        return gson.toJson(strings);
    }
}
// END
