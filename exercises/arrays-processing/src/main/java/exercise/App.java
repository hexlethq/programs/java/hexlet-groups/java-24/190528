package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getSumBeforeMinAndMax(int[] arr) {
        int minIndex = getMinValueIndex(arr);
        int maxIndex = getMaxValueIndex(arr);
        int sum = 0;
        for (int i = Math.min(minIndex, maxIndex) + 1; i < Math.max(minIndex, maxIndex); i++) {
            sum += arr[i];
        }
        return sum;
    }

    public static int[] getElementsLessAverage(int[] arr) {
        int sum = 0;
        int countElement = 0;
        int average;
        for (int a : arr) {
            sum = sum + a;
            countElement++;
        }

        if (countElement != 0) {
            average = sum / countElement;
        } else {
            return arr;
        }

        int[] newArr = new int[arr.length];
        int count = 0;
        for (int j : arr) {
            if (j <= average) {
                newArr[count] = j;
                count++;
            }
        }
        return Arrays.copyOf(newArr, count);
    }

    static int getMinValueIndex(int[] arr) {
        int min = arr[0];
        int indexMinElement = 0;
        for (int i = 1; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
                indexMinElement = i;
            }
        }
        return indexMinElement;
    }

    static int getMaxValueIndex(int[] arr) {
        int max = arr[0];
        int indexMaxElement = 0;
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
                indexMaxElement = i;
            }
        }
        return indexMaxElement;
    }
    // END
}
