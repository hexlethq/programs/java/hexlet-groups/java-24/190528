package exercise.controller;

import exercise.dto.AllWeather;
import exercise.dto.OnlyTemperature;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public AllWeather getWeather(@PathVariable long id) {
        return weatherService.getCityWeather(id);
    }

    @GetMapping(path = "search")
    public List<OnlyTemperature> findByName(@RequestParam("name") Optional<String> name) {
        return this.weatherService.getAllCitiesWeather(name);
    }

    // END
}

