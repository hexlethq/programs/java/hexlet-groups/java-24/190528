package exercise.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.HttpClient;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import exercise.dto.AllWeather;
import exercise.dto.OnlyTemperature;
import org.springframework.stereotype.Service;
import exercise.CityNotFoundException;
import exercise.repository.CityRepository;
import exercise.model.City;
import org.springframework.beans.factory.annotation.Autowired;


@Service
public class WeatherService {

    @Autowired
    CityRepository cityRepository;

    // Клиент
    HttpClient client;

    // При создании класса сервиса клиент передаётся снаружи
    // В теории это позволит заменить клиент без изменения самого сервиса
    WeatherService(HttpClient client) {
        this.client = client;
    }

    // BEGIN
    private City getCity(long id) {
        return this.cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City not found"));
    }

    private List<City> getAllOrdered() {
        return this.cityRepository.findAllByOrderByNameAsc();
    }

    private List<City> findByName(String name) {
        return this.cityRepository.findAllByNameStartingWithIgnoreCase(name);
    }

    private AllWeather getWeather(City city) {
        String url = String.format("http://weather/api/v2/cities/%s", city.getName());
        String data = this.client.get(url);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(data, AllWeather.class);

        } catch (JsonProcessingException e) {
            throw new RuntimeException();
        }

    }

    public AllWeather getCityWeather(long id) {
        City city = this.getCity(id);
        return this.getWeather(city);
    }

    public List<OnlyTemperature> getAllCitiesWeather(Optional<String> name) {
        List<City> cities = name
                .map(this::findByName)
                .orElse(this.getAllOrdered());
        return cities.stream()
                .map(this::getWeather)
                .map(OnlyTemperature::new)
                .collect(Collectors.toList());

    }
    // END
}
