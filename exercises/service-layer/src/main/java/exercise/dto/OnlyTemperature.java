package exercise.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnlyTemperature {
    private String name;
    private String temperature;

    public OnlyTemperature(AllWeather allWeather) {
        this.name = allWeather.getName();
        this.temperature = allWeather.getTemperature();
    }
}
