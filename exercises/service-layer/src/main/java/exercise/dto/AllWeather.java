package exercise.dto;

import lombok.Data;

@Data
public class AllWeather {
    private String name;
    private String temperature;
    private String cloudy;
    private String wind;
    private String humidity;
}
