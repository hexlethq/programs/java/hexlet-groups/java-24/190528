package exercise;

import java.util.Arrays;

// BEGIN
public class MaxThread extends Thread {

    private final int[] arr;
    private int max;

    public MaxThread(int[] arr) {
        this.arr = arr;
    }

    public int getMax() {
        return max;
    }

    @Override
    public void run() {
        max = Arrays.stream(arr).max().getAsInt();
    }
}
// END
