package exercise;

import java.util.Arrays;

// BEGIN
public class MinThread extends Thread {
    private final int[] arr;
    private int min;

    public MinThread(int[] arr) {
        this.arr = arr;
    }

    public int getMin() {
        return min;
    }

    @Override
    public void run() {
        min = Arrays.stream(arr).min().getAsInt();
    }
}
// END
