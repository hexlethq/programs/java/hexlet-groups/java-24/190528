package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static void main(String[] args) {
        int[] arr = {4, 0, -2, 1, 0, 12123, -211};
        System.out.println(getMinMax(arr));
    }

    public static Map<String, Integer> getMinMax(int[] arr) {
        Map<String, Integer> result = new HashMap<>();

        MaxThread maxThread = new MaxThread(arr);
        maxThread.start();
        LOGGER.info("Thread " + maxThread.getName() + " start");
        MinThread minThread = new MinThread(arr);
        minThread.start();
        LOGGER.info("Thread " + minThread.getName() + " start");

        try {
            maxThread.join();
            LOGGER.info("Thread " + maxThread.getName() + " stopped");

            minThread.join();
            LOGGER.info("Thread " + minThread.getName() + " stopped");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        result.put("max", maxThread.getMax());
        result.put("min", minThread.getMin());
        return result;
    }
    // END
}

