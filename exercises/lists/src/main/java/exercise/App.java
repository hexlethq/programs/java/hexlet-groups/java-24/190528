package exercise;

import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {

    public static boolean scrabble(final String symbols, final String word) {
        int wordLength = word.length();
        int symbolLength = symbols.length();

        if (symbolLength < wordLength) {
            return false;
        }

        String wordToLowerCase = word.toLowerCase();
        List<Character> listOfWordCharacters = wordToLowerCase.chars()
                .mapToObj((i) -> (char) i).collect(Collectors.toList());

        List<Character> listOfSymbolCharacters = symbols.chars()
                .mapToObj((i) -> (char) i).collect(Collectors.toList());

        for (Character character : listOfSymbolCharacters) {
            listOfWordCharacters.remove(character);
            if (listOfWordCharacters.isEmpty()) {
                return true;
            }
        }

        return listOfWordCharacters.isEmpty();
    }
}
//END
