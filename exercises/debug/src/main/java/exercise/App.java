package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        String str;
        if (((a + b) > c) && ((b + c) > a) && ((a + c) > b)) {
            if (a != b && b != c && a != c) {
                str = "Разносторонний";
            } else if (a == b && b == c) {
                str = "Равносторонний";
            } else {
                str = "Равнобедренный";
            }

        } else {
            str = "Треугольник не существует";
        }
        return str;
    }
    // END
}
