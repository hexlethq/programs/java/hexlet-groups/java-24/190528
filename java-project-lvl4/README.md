[![Actions Status](https://github.com/saymon-says/java-project-lvl4/workflows/hexlet-check/badge.svg)](https://github.com/saymon-says/java-project-lvl4/actions)
[![Actions Status](https://github.com/saymon-says/java-project-lvl4/workflows/my-project-check/badge.svg)](https://github.com/saymon-says/java-project-lvl4/actions)
[![Maintainability](https://api.codeclimate.com/v1/badges/6bc70b8e4b4d35bef182/maintainability)](https://codeclimate.com/github/saymon-says/java-project-lvl4/maintainability)
[![Test Coverage](https://api.codeclimate.com/v1/badges/6bc70b8e4b4d35bef182/test_coverage)](https://codeclimate.com/github/saymon-says/java-project-lvl4/test_coverage)

[![wakatime](https://wakatime.com/badge/user/7878f337-eef6-4efa-9195-656132baf3c5/project/ba895f85-d960-465f-9721-24bb1c6f44a5.svg)](https://wakatime.com/badge/user/7878f337-eef6-4efa-9195-656132baf3c5/project/ba895f85-d960-465f-9721-24bb1c6f44a5)

Заходи
https://page-analize.herokuapp.com/
